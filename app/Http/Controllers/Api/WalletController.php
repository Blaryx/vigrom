<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class WalletController extends Controller
{
    public function update(Request $request)
    {
        $rules = [
            'wallet_id'     => ['required','integer'],
            'value'         => ['required','numeric','gt:0'],
            'transaction'   => ['required','string',Rule::in(Wallet::TRANSACTION_ARR)],
            'currency'      => ['required','string',Rule::in(Wallet::CURRENCY_ARR)],
            'reason'        => ['required','string',Rule::in(Wallet::REASON_ARR)],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([$validator->getMessageBag()], Response::HTTP_BAD_REQUEST);
        }

        $params = $request->input();

        $wallet = Wallet::findOrFail($params['wallet_id']);

        try {

            $wallet->makeTransaction($params);

        } catch (\Exception $e) {

            return response()->json([$e->getMessage()], Response::HTTP_EXPECTATION_FAILED);
        }

        return response($wallet->toArray());
    }

    public function get(Request $request)
    {
        $rules = [
            'wallet_id' => 'required|integer',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([$validator->getMessageBag()], Response::HTTP_BAD_REQUEST);
        }

        $wallet = Wallet::findOrFail($request->input('wallet_id'));

        return response($wallet->balance . ' ' . $wallet->currency);
    }
}
