<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Exception;

class Wallet extends Model
{
    use HasFactory;

    const CURRENCY_USD = 'USD';
    const CURRENCY_RUB = 'RUB';

    const CURRENCY_ARR = [
        self::CURRENCY_USD,
        self::CURRENCY_RUB,
    ];

    const TRANSACTION_DEBIT = 'debit';
    const TRANSACTION_CREDIT= 'credit';

    const TRANSACTION_ARR = [
        self::TRANSACTION_DEBIT,
        self::TRANSACTION_CREDIT,
    ];

    const REASON_STOCK  = 'stock';
    const REASON_REFUND = 'refund';

    const REASON_ARR = [
        self::REASON_STOCK,
        self::REASON_REFUND,
    ];

    public function makeTransaction($params)
    {
        DB::transaction(function () use ($params) {

            $rate = 1;
            if ($params['currency'] !== $this->currency) {
                $rate = Currency::getRate($params['currency'], $this->currency);
            }

            $convertedValue = floor($params['value'] * $rate * 100) / 100;

            $this->balance = $params['transaction'] === self::TRANSACTION_DEBIT ?
                $this->balance + $convertedValue :
                $this->balance - $convertedValue;

            if ($this->balance < 0) {
                throw new Exception('Not enough funds');
            }

            $this->save();

            $params['rate']             = $rate;
            $params['result_balance']   = $this->balance;

            WalletLog::create($params);
        });

        return $this;
    }

    public function getBalanceAttribute($value)
    {
        return round($value * 100) / 100;
    }
}
