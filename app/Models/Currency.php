<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Exception;

class Currency extends Model
{
    use HasFactory;

    static function getRate($from, $to)
    {
        $currencies = self::whereIn('code', [$from, $to])->get();

        if ($currencies->count() !== 2) {
            throw new Exception('Exchange rate not found');
        }

        $currencyFrom   = $currencies->firstWhere('code', $from);
        $currencyTo     = $currencies->firstWhere('code', $to);

        return $currencyTo->exchange_rate / $currencyFrom->exchange_rate;
    }
}
