<?php

use App\Models\Wallet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->decimal('balance',19,2);
            $table->string('currency',3)->default(Wallet::CURRENCY_USD);
            $table->timestamps();
        });

        DB::table('wallets')->insert([
            [
                'user_id'           => 241,
                'balance'           => 100,
                'currency'          => Wallet::CURRENCY_USD,
                'created_at'        => Carbon::yesterday(),
                'updated_at'        => Carbon::yesterday(),
            ],
            [
                'user_id'           => 242,
                'balance'           => 200,
                'currency'          => Wallet::CURRENCY_RUB,
                'created_at'        => Carbon::yesterday(),
                'updated_at'        => Carbon::yesterday(),
            ],
            [
                'user_id'           => 243,
                'balance'           => 300,
                'currency'          => Wallet::CURRENCY_USD,
                'created_at'        => Carbon::today(),
                'updated_at'        => Carbon::today(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
