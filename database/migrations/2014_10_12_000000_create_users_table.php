<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            [
                'id'                => 241,
                'name'              => 'Vasya',
                'email'             => 'vasya@domain.com',
                'email_verified_at' => Carbon::yesterday(),
                'password'          => Hash::make(Str::random(10)),
                'created_at'        => Carbon::yesterday(),
                'updated_at'        => Carbon::yesterday(),
            ],
            [
                'id'                => 242,
                'name'              => 'Petya',
                'email'             => 'petya@domain.com',
                'email_verified_at' => Carbon::yesterday(),
                'password'          => Hash::make(Str::random(10)),
                'created_at'        => Carbon::yesterday(),
                'updated_at'        => Carbon::yesterday(),
            ],
            [
                'id'                => 243,
                'name'              => 'Kolya',
                'email'             => 'kolya@domain.com',
                'email_verified_at' => Carbon::yesterday(),
                'password'          => Hash::make(Str::random(10)),
                'created_at'        => Carbon::today(),
                'updated_at'        => Carbon::today(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
