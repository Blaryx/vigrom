# Vigrom Test task

## Installation

- Clone repo
- Create database
- Copy .env.example to .env
- Add APP_URL and database params
- run
```bash
composer install
php artisan optimize
php artisan migrate
php artisan currency:manage add USD,RUB
```

- Currency rate update periodically - add to cron
```bash
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1

```
- Currency rate update once - run
```bash
php artisan currency:update -o
```
- Open project in browser
